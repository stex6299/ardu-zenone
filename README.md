# What's here
In questo repository c'è un esempio di come si possa aumentare il numero di bit salvati in un arduino uno, dichiarando un array di tipo `byte` e sfruttando ciascun bit

- [vectorGenerator.py](./vectorGenerator.py) permette di generare la stringa per la dichiarazione dell'array `const byte stato [128] = { ... }`
- [ardu.ino](./ardu/ardu.ino) sketch arduino per accedere a tutti i vari stati, sia in ordine naturale (nel `setup`) che a partire da un elemento random (nel `loop`)
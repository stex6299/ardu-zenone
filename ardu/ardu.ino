/*
Example of a sketch to increase the stored numbers in a vector by exploiting every single bit

The vector has been created with vectorGenerator.py
*/

const byte stato [128] = { 0b00100010, 0b10011010, 0b11010011, 0b00100010, 0b01001010, 0b00010101, 0b11110001, 0b11101001, 0b10000110, 0b01010010, 0b10001110, 0b11001100, 0b11001010, 0b00100100, 0b10101001, 0b10011001, 0b01101101, 0b00110111, 0b11001011, 0b01000001, 0b00001110, 0b00111010, 0b11100110, 0b11000011, 0b00100011, 0b10010111, 0b11011101, 0b11110000, 0b01101101, 0b00101100, 0b10001100, 0b01000100, 0b10101111, 0b10000000, 0b01101100, 0b00010000, 0b00100110, 0b10001100, 0b01011101, 0b10110111, 0b10001011, 0b00010010, 0b01000011, 0b00011000, 0b01111001, 0b10010011, 0b00101110, 0b11110010, 0b01111101, 0b10101010, 0b01111001, 0b10101110, 0b00111011, 0b01011001, 0b01011100, 0b01001001, 0b00100010, 0b01010110, 0b10110111, 0b01010001, 0b00001110, 0b01010101, 0b01010011, 0b11111011, 0b11111000, 0b01111100, 0b10111001, 0b11110001, 0b00011101, 0b01001111, 0b10101011, 0b11110101, 0b10001100, 0b01100111, 0b10100110, 0b10101000, 0b10111111, 0b01111101, 0b10011100, 0b01101011, 0b10010000, 0b00011011, 0b11011001, 0b10101011, 0b10101001, 0b01100000, 0b00010011, 0b10010000, 0b11111000, 0b11001100, 0b11111101, 0b01011110, 0b11111101, 0b00101111, 0b00100100, 0b10101100, 0b00010001, 0b00110111, 0b11010111, 0b11111111, 0b00111101, 0b00111110, 0b11111010, 0b11101110, 0b01010010, 0b00011010, 0b11111110, 0b00111011, 0b00000010, 0b01100110, 0b10001010, 0b00101101, 0b11001011, 0b00101011, 0b10111001, 0b11110100, 0b10000011, 0b10010000, 0b00111100, 0b11010101, 0b00110011, 0b01011111, 0b11101010, 0b11011110, 0b01010010, 0b01100010, 0b01001011, 0b10010101, };

void setup() {

    Serial.begin(9600);

    /*EXAMPLE OF PRINTING THE WHOLE VECTOR*/

    // Loop over array elements
    for (int i = 0; i < (sizeof(stato)/sizeof(byte)); i++){
        // Loop over bits of an element (from LSB to MSB i.e. dx to sx)
        for (byte n = 0; n < 8; n++){
            // Get the nth bit of the ith element of the vector
            Serial.println((stato[i] >> n) & 0x1);
        }
        Serial.println("-----------");
        
    }


}




void loop() {

    /*EXAMPLE OF CONTINUOSLY LOOPING ON THE STATES FROM A RANDOM POSITION*/


    // Extract somehow both the starting n and i
    int i = random(0, (sizeof(stato)/sizeof(byte)));
    int n = random(0, 8);


    // Fro a byte, n should be between 0 and 7
    if (n > 7){
        // Restart from the LSB
        n = 0;
        
        // Move to next element of the array
        i++;

        if (! (i < (sizeof(stato)/sizeof(byte)))){
            i = 0;
        }
    }

    // Here's the state you want to use
    Serial.println((stato[i] >> n) & 0x1);

    // Move to next bit
    n++;


    /*
    // Loop continuously
    while (i < (sizeof(stato)/sizeof(byte))){
        
        while (n < 8){

            // Here's the state you want to use
            Serial.println((stato[i] >> n) & 0x1);

            n++;
        }
        // Restart from the LSB of the next elements
        n = 0;
        
        i++;
    }

    // Restart from the beginning of the array
    n = 0;
    */

}


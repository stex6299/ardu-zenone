"""
Scopo di questo programma è generarsi un vettore di 1024 numeri casuali 0/1
codificati sotto forma di 128 interi a 8 bit

L'output è pronto per essere pastato in un arduino IDE
"""

#%%
from numpy import random

# Genero 1024 numeri random 0/1
vect = random.randint(0, 2, 1024)


#%%

print(f"const byte stato [{vect.shape[0]//8}] = ", end = "")
print("{ ", end = "")
for i in range(vect.shape[0]//8):
    
    # print("B", end = "") # Uses constants of bynary.h (only for 8 bits)
    print("0b", end = "")
    for n in range(8):
        # print(vect[i] & 1 << n)
        print(vect[8*i + n], end = "")
        
    print(", ", end = "")
        
print("};")
